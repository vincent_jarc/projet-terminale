# Titre du jeu

[[_TOC_]]

## Description

(_Décrire succinctement le jeu._)

(_insérer un image_)

***

## Membres du projet

- Prénom NOM (Chef de projet)
- Prénom NOM (Développeur)
- Prénom NOM (Développeur)

***

## Version

Le jeu est actuellement en version… (_à compléter_)

***

## Technologies

Le jeu utilise Python en version 3.# (_à compléter_) ou supérieur ainsi que les bibliothèques suivantes : 

- Tkinter
- (_à compléter_)

***

## Installation

_Décrire la procédure d'installation ici._